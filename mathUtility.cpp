/***************************************************************************** 
 *  \author 
 *  	Dr. Fares Abu-Dakka
 *
 *  \version 
 *	V1.0
 *
 *  \date $Date: 2010-2015$
 *
 *  \contact: fabudakk@ing.uc3m.es
 *
 *  \file   mathUtility.cpp
 */
#include "mathUtility.hpp"

std::chrono::high_resolution_clock::time_point Timing::_tic = std::chrono::high_resolution_clock::now();

namespace math{

//-------------------------------------------------------------------------------------------
Real drand (Real min, Real max, int seed, int n) {
	if ( seed >= 0 )
		srand(seed);
	long k=1;
	long r = ((long)rand() << 15) | rand();
	for (int i=0;i<n; i++)
		k*=10;
	return ((r % (int)(max*k - min*k + 1)) + (int)(min*k)) / (Real) k;
}
//-------------------------------------------------------------------------------------------
Real Simpledrand(Real a, Real b, int seed) {
	if ( seed >= 0 )
		srand(seed);
	Real random = ((Real) rand()) / (Real) RAND_MAX;
	Real diff = b - a;
	Real r = random * diff;
	return a + r;
}
//-------------------------------------------------------------------------------------------
Real signof(Real a){
	//calculates the SIGNOF function
	//return	: SIGNOF(a)
	if (a > 0)		return  1.0;
	else
		if (a < 0)	return -1.0;
		else		return  0.0;
}

};
