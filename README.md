/***************************************************************************** 
 *  \author 
 *  	Dr. Fares Abu-Dakka
 *
 *  \version 
 *	V1.0
 *
 *  \date $Date: 2010-2015$
 *
 *  \contact: fabudakk@ing.uc3m.es
 *
 *  \file   mathUtility.hpp
 *     1) provides some general functions and macro definitions
 *     2) randome generation functions
 *     3) Timing class: provides a simple tool for computation time calculations
 *        time can be calculated in nano-, micro-, mili- seconds and in seconds
 *        the posibility to use tic toc functions as in matlab.
 *        tic; ....your code.....; toc;
 */

/**************************************************************************
*   This library is free software; you can redistribute it and/or         *
*   modify it under the terms of the GNU Lesser General Public            *
*   License as published by the Free Software Foundation; either          *
*   version 2.1 of the License, or (at your option) any later version.    *
*                                                                         *
*   This library is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU     *
*   Lesser General Public License for more details.                       *
*                                                                         *
*   You should have received a copy of the GNU Lesser General Public      *
*   License along with this library; if not, write to the Free Software   *
*   Foundation, Inc., 59 Temple Place,                                    *
*   Suite 330, Boston, MA  02111-1307  USA                                *
*                                                                         *
***************************************************************************/
